var map = {
    cols: 24,
    rows: 50,
    tsize: 64,
    layers: [[
         1 ,  2,  2,  2,  2,  2,  2,  2,  2,  2,  3,400,400,  1,  2,  2,  2 , 2,  2,  2,  2,  2,  2,  3,
         17, 18, 18, 18, 18, 18, 18, 18, 20, 18, 19,400,400, 17,  8, 18, 21, 18, 37, 53, 18, 18, 18, 19,
         33, 34, 34, 35, 33, 35, 33, 34, 36, 33, 33,  1,  3, 33, 33, 33, 86, 33, 33, 33, 33, 33, 33, 33,
         51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 18, 18, 51, 97, 99, 98, 98, 99, 98, 99,100, 51, 51, 
          9,  9,  9,  9,  9,  9,  9, 51, 51, 51, 51, 51, 51, 51,113,114,114,114,114,114,114,116, 51, 51,
        400,400,400,400,400,400,400, 51, 51, 51, 51, 51, 51, 51,129,130,131,130,131,131,131,132, 51, 51, 
        400,400,400,400,400,400,400,100, 51, 97, 99, 99,100, 51,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,
        400,400,400,400,400,400,400,116, 51,113,114,114,116, 97,400,400,400,400,400,400,400,400,400,400,
        400,  1,  2,  2,  2,  2,  3, 51, 51,113,114,114,116, 97,400,400,400,400,400,400,400,400,400,400, 
        400, 17, 37, 18, 65, 18, 21,116, 51,113,114,114,116,129,400,400,400,400,400,400,400,400,400,400, 
        400, 51, 51, 51, 81,131, 51, 51, 51,129,130,130,132, 51,  4, 18, 18, 53, 54, 18, 21, 18, 19,400,
        400, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 33, 34, 34, 35, 33, 33, 34, 34, 35,400,
        400,  9,  9,  9,  9,  9,  9, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 51, 97,400, 
        400,400,400,400,400,400,400, 51, 51, 51, 51,  9,  9,  9,  9,  9,  9,  9,  9,  9, 51, 51, 51,400,
        400,400,400,400,400,400,400, 51, 51, 51,113,400,400,400,400,400,400,400,400,400,116, 51, 51,400,
        400,400,400,400,400,400,400,132, 51, 51,113,400,400,400,  1,  2,  3,400,400,400,132, 51, 51,400, 
        400,400,400,400,400,400,400,100, 51, 51, 51,400,400,400, 17, 21, 19,400,400,400, 51, 51, 51,400,
        400,400,400,400,400,400,400,100, 51, 51,129,400,400,400, 33, 33, 33,400,400,400, 51, 51, 51,400,
        400,400,400,400,400,400,400, 99,100, 51, 51,400,400,400, 51, 51, 97,400,400,400,100, 51, 97,400, 
        400,400,400,400,400,400,400,131,132, 51,129,400,400,400, 51, 51,113,400,400,400,132, 51,113,400,
        400,400,400,400,400,400,400, 51, 51, 51, 97,400,400,400,116, 51,113,400,400,400,116, 51,113,400,
        400,400,400,400,400,400,400, 51, 51, 51, 97,400,400,400,116, 51, 51,400,400,400, 51, 51, 97,400, 
        400,400,400,400,400,400,400, 51,  6,  7, 51,400,400,400,132, 51, 51, 34, 33, 34, 51, 51, 51,400,
        400,400,400,400,400,400,400,222, 22, 23,222,400,400,400, 51, 51, 51, 51, 51, 51, 51, 51, 51,400,
        400,400,400,400,400,400,400,400,400,400,400,400,400,400,  9,  9,  9,  9,  9,  9,  9,  9,  9,400,
        400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400
    ], [
        139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
        123,124, 18, 18, 18, 18, 18, 18, 20, 18,153,  0,  0,154,  0,  0,  0,  0,  0,  0,  0, 18,141,142,
        123,124,  0,  0,  0,  0,  0,  0,  0,  0,174,  1,  3,174,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
        139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0, 18, 18,  0,  0,  0,  0,  0,  0,  0,  0,  0,141,142,
        139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0, 50,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  9,  9,  9,  9,  9,  9,  9,  9,125,126,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,123,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  2,  2,  2,  2,  2,  2,  3, 0,
         0,123,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,158, 0,
         0,123,124, 0,160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,174, 0,
         0,139,140,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 158, 0, 50, 0, 0, 0, 0, 0,
         0,  0,  0,  0,  0, 0,  0,154, 0, 0,155, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,174, 0, 0,174, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,160, 0, 0,159, 0, 0, 0,50, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,174, 0, 0,174, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,  2,  3, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,190, 0, 0,190, 0, 0, 0, 0, 0, 0, 17, 18, 19, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,206, 0, 0,206, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 143,144,144,144, 0, 0, 0,159, 0, 0, 0, 0, 0, 0, 0, 158, 0,
    ], [
        139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
        123,124, 18, 18, 18, 18, 18, 18, 20, 18,153,  0,  0,154,  0,  0,  0,  0,  0,  0,  0, 18,141,142,
        123,124,  0,  0,  0,  0,  0,  0,  0,  0,174,  1,  3,174,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
        139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0, 18, 18,  0,  0,  0,  0,  0,  0,  0,  0,  0,141,142,
        139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,125,126,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  9,  9,  9,  9,  9,  9,  9,  9,125,126,
         0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,123,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
         0,139,140,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  2,  2,  2,  2,  2,  2,  3, 0,
         0,123,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,158, 0,
         0,123,124, 0,160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,174, 0,
         0,139,140,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 158, 0, 0, 0, 0, 0, 0, 0,
         0,  0,  0,  0,  0, 0,  0,154, 0, 0,155, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,174, 0, 0,174, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0,  0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,160, 0, 0,159, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,174, 0, 0,174, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,  2,  3, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,190, 0, 0,190, 0, 0, 0, 0, 0, 0, 17, 18, 19, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0,206, 0, 0,206, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0,  0,  0, 0, 143,144,144,144, 0, 0, 0,159, 0, 0, 0, 0, 0, 0, 0, 158, 0,
    ]],
    getTile: function (layer, col, row) {
        return this.layers[layer][row * map.cols + col];
    },
    isSolidTileAtXY: function (x, y) {
        var col = Math.floor(x / (this.tsize));
        var row = Math.floor(y / (this.tsize));

        // tiles 3 and 5 are solid -- the rest are walkable
        // loop through all layers and return TRUE if any tile is solid
        return this.layers.reduce(function (res, layer, index) {
            var tile = this.getTile(index, col, row);
            var isSolid = tile === 1 || tile === 2 || tile === 3 || tile === 5 || tile === 9 || tile === 17 || tile === 18 || tile === 19 || tile === 81 || tile === 174 || tile === 190 || tile === 206 || tile === 222 ||  tile === 400;
            return res || isSolid;
        }.bind(this), false);
    },
    getCol: function (x) {
        return Math.floor(x / (this.tsize));
    },
    getRow: function (y) {
        return Math.floor(y / (this.tsize));
    },
    getX: function (col) {
        return col * this.tsize;
    },
    getY: function (row) {
        return row * this.tsize;
    }
};

function Camera(map, width, height) {
    this.x = 0;
    this.y = 0;
    this.width = width;
    this.height = height;
    this.maxX = map.cols * map.tsize - width;
    this.maxY = map.rows * map.tsize - height;
}

Camera.prototype.follow = function (sprite) {
    this.following = sprite;
    sprite.screenX = 0;
    sprite.screenY = 0;
};

Camera.prototype.update = function () {
    // assume followed sprite should be placed at the center of the screen
    // whenever possible
    this.following.screenX = this.width / 2;
    this.following.screenY = this.height / 2;

    // make the camera follow the sprite
    this.x = this.following.x - this.width / 2;
    this.y = this.following.y - this.height / 2;
    // clamp values
    this.x = Math.max(0, Math.min(this.x, this.maxX));
    this.y = Math.max(0, Math.min(this.y, this.maxY));

    // in map corners, the sprite cannot be placed in the center of the screen
    // and we have to change its screen coordinates

    // left and right sides
    if (this.following.x < this.width / 2 ||
        this.following.x > this.maxX + this.width / 2) {
        this.following.screenX = this.following.x - this.x;
    }
    // top and bottom sides
    if (this.following.y < this.height / 2 ||
        this.following.y > this.maxY + this.height / 2) {
        this.following.screenY = this.following.y - this.y;
    }
};

function Hero(map, x, y) {
    this.map = map;
    this.x = x;
    this.y = y;
    this.width = 40;
    this.height = map.tsize;

    this.image = Loader.getImage('hero');
}

Hero.SPEED = 512; // pixels per second

Hero.prototype.move = function (delta, dirx, diry) {
    // move hero
    this.x += dirx * Hero.SPEED * delta;
    this.y += diry * Hero.SPEED * delta;

    // check if we walked into a non-walkable tile
    this._collide(dirx, diry);

    // clamp values
    var maxX = this.map.cols * this.map.tsize;
    var maxY = this.map.rows * this.map.tsize;
    this.x = Math.max(0, Math.min(this.x, maxX));
    this.y = Math.max(0, Math.min(this.y, maxY));
};

Hero.prototype._collide = function (dirx, diry) {
    var row, col;
    // -1 in right and bottom is because image ranges from 0..63
    // and not up to 64
    var left = this.x - this.width / 2;
    var right = this.x + this.width / 2 - 1;
    var top = this.y - this.height / 2;
    var bottom = this.y + this.height / 2 - 1;

    // check for collisions on sprite sides
    var collision =
        this.map.isSolidTileAtXY(left, top) ||
        this.map.isSolidTileAtXY(right, top) ||
        this.map.isSolidTileAtXY(right, bottom) ||
        this.map.isSolidTileAtXY(left, bottom);
    if (!collision) { return; }

    if (diry > 0) {
        row = this.map.getRow(bottom);
        this.y = -this.height / 2 + this.map.getY(row);
    }
    else if (diry < 0) {
        row = this.map.getRow(top);
        this.y = this.height / 2 + this.map.getY(row + 1);
    }
    else if (dirx > 0) {
        col = this.map.getCol(right);
        this.x = -this.width / 2 + this.map.getX(col);
    }
    else if (dirx < 0) {
        col = this.map.getCol(left);
        this.x = this.width / 2 + this.map.getX(col + 1);
    }
};

Game.load = function () {
    return [
        Loader.loadImage('tiles', '../assets/tiles.png'),
        Loader.loadImage('hero', '../assets/character.png')
    ];
};

Game.init = function () {
    Keyboard.listenForEvents(
        [Keyboard.LEFT, Keyboard.RIGHT, Keyboard.UP, Keyboard.DOWN]);
    this.tileAtlas = Loader.getImage('tiles');
    this.heroAtlas = Loader.getImage('hero');

    this.hero = new Hero(map, 160, 160);
    this.camera = new Camera(map, 1280, 1024);
    this.camera.follow(this.hero);
};

Game.update = function (delta) {
    // handle hero movement with arrow keys
    var dirx = 0;
    var diry = 0;
    if (Keyboard.isDown(Keyboard.LEFT)) { dirx = -1; }
    else if (Keyboard.isDown(Keyboard.RIGHT)) { dirx = 1; }
    else if (Keyboard.isDown(Keyboard.UP)) { diry = -1; }
    else if (Keyboard.isDown(Keyboard.DOWN)) { diry = 1; }

    this.hero.move(delta, dirx, diry);
    this.camera.update();
};

Game._drawLayer = function (layer) {
    var startCol = Math.floor(this.camera.x / map.tsize);
    var endCol = startCol + (this.camera.width / map.tsize);
    var startRow = Math.floor(this.camera.y / map.tsize);
    var endRow = startRow + (this.camera.height / map.tsize);
    var offsetX = -this.camera.x + startCol * map.tsize;
    var offsetY = -this.camera.y + startRow * map.tsize;

    for (var c = startCol; c <= endCol; c++) {
        for (var r = startRow; r <= endRow; r++) {
            var tile = map.getTile(layer, c, r);
            var x = (c - startCol) * map.tsize + offsetX;
            var y = (r - startRow) * map.tsize + offsetY;
            if (tile !== 0) { // 0 => empty tile
                this.ctx.drawImage(
                    this.tileAtlas, // image
                    (((tile-1)%16)*64),  // source x
                    (Math.floor((tile-1)/16)*64), // source y
                    map.tsize, // source width
                    map.tsize, // source height
                    Math.round(x),  // target x
                    Math.round(y), // target y
                    map.tsize, // target width
                    map.tsize // target height
                );
            }
        }
    }
};


Game.render = function () {
    // draw map background layer
    this._drawLayer(0);

    // draw map top layer
    this._drawLayer(1);

    // draw main character
    this.ctx.drawImage(
        this.heroAtlas, //image
        0,  // source x
        0, // source y
        40, // source width
        64, // source height
        this.hero.screenX - 40,
        this.hero.screenY - 64,
        64, // target width
        64 // target height

    );
    // draw map top layer
    this._drawLayer(2);
};
